MARK ?= not slowtest
KEYWORD ?= test
ARGS ?=

test: activate-service-account
	pytest -m "$(MARK)" -k "$(KEYWORD)" $(ARGS)

slowtest:
	$(MAKE) MARK='slowtest' test

# ----------------------------------------------------------------------------
# The following variables and targets are present to try to clean up and
# recreate the pipeline script in Bitbucket.
# ----------------------------------------------------------------------------

# These variables may be overridden by environment variables with the same name
REPO_NAME ?= rhedcloud-gcp-org-dm
PROJECT_ID ?= gcp-project-1-257718
DEPLOYMENT_NAME ?= rhedcloud-gcp-org
BITBUCKET_BUILD_NUMBER ?= 0
GOOGLE_APPLICATION_CREDENTIALS ?= /tmp/key_file
BB_TEAM ?= rhedcloud

save-creds:
	save_api_keys.sh

activate-service-account:
	gcloud auth activate-service-account --key-file "$(GOOGLE_APPLICATION_CREDENTIALS)"
	gcloud config set project $(PROJECT_ID)
	$(MAKE) whoami

whoami:
	gcloud config list account --format "value(core.account)"

# # Download the artifacts required for the tests in this repository
# get-artifacts:
# 	download_artifact.sh rhedcloud-gcp-rs-project-dm rhedcloud-gcp-rs-project-dm.latest.zip

# Retrieve necessary artifacts and run a sequence of targets to clean and
# rebuild our stacks.
# rebuild: get-artifacts clean rs-project vpc-type1
rebuild: clean org

# Run a sequence of targets to clean and rebuild our stacks.
local-rebuild: clean org

# Clean up resources if stack changes are detected
clean: activate-service-account
	delete_deployment.py "$(DEPLOYMENT_NAME)"

# Create the org deployment
org: activate-service-account working-org
	cat "$(DEPLOYMENT_NAME)-dm-working.json"
	gcloud deployment-manager deployments create "$(DEPLOYMENT_NAME)" \
		--project "$(PROJECT_ID)" \
		--config "$(DEPLOYMENT_NAME)-dm-working.json"
	gcloud beta resource-manager org-policies set-policy org-external-ip-policy.json --organization $(ORG_ID)
	gcloud beta resource-manager org-policies set-policy org-no-def-net-policy.json --organization $(ORG_ID)
	gcloud beta resource-manager org-policies set-policy hipaa-resource-location.json --folder 681006986517

# Replace various placeholders in the deployment template with the appropriate values.
working-org:
	sed -e "s/BITBUCKET_BUILD_NUMBER/$(BITBUCKET_BUILD_NUMBER)/g" \
		-e "s/ORG_ID/$(ORG_ID)/g" \
		"$(DEPLOYMENT_NAME)-dm.json" > "$(DEPLOYMENT_NAME)-dm-working.json"

# Upload test artifacts
upload:
	git archive --format zip --output $(REPO_NAME).latest.zip master
	cp $(REPO_NAME).json $(REPO_NAME).latest.json
	upload_artifact.sh \
		org-external-ip-policy.json \
		org-no-def-net-policy.json \
		hipaa-resource-location.json \
		$(REPO_NAME).latest.json \
		$(REPO_NAME).latest.zip

.EXPORT_ALL_VARIABLES:
