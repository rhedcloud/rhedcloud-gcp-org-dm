{
    "resources": [
      {
        "name": "custom-security-role",
        "type": "gcp-types/iam-v1:organizations.roles",
        "metadata": {
          "comment": {
            "Metadata": {
                "Tester": "Unassigned",
                "Purpose": "RHEDcloudSecurityIRRole will be assumed by Central Security Response administrators of the AWS Account after they authenticate via SAML with IDP.",
                "Description": "The RHEDcloudSecurityIRRole should allow all operations required for security incident response.  This role will be used by Enterprise Security team to respond to secuirty incidents.",
                "TestStrategy": "The test strategy for the RHEDcloudSecurityIRRole is to verify that the role exists, verify that the expected inline policy is attached to this role, and then implement test cases that assume this role and perform specific allowed operations.",
                "Tests": {
                    "Test": [
                        {
                            "Type": "structural",
                            "Name": "test_security_ir_role_info.test_exists",
                            "Description": "Verify that the RHEDcloudSecurityIRRole exists",
                            "Plan": "Look up the role by name and compare the ARN to the expected result",
                            "ExpectedResult": "Success"
                        },
                        {
                            "Type": "structural",
                            "Name": "test_security_ir_role_info.test_attached_policies",
                            "Description": "Verify that the x, y and z permissions are associated to the RHEDcloudSecurityIRRole.",
                            "Plan": "Look up the role by name and inspect the list of associated permissions.",
                            "ExpectedResult": "Success"
                        },
                        {
                            "Type": "functional",
                            "Name": "test_security_ir_role",
                            "Description": "This test performs a series of positive and negative tests to verify the Security IR administrator role.",
                            "Plan": "Create a test user, attach the same inline policy to the test user, and perform a series of negative and positive tests to ensure the role is working as expected.",
                            "ExpectedResult": "Success"
                        }
                    ]
                }
              }
            }
        },
        "properties": {
          "parent": "organizations/ORG_ID",
          "roleId": "RHEDcloudSecurityIRRole_BITBUCKET_BUILD_NUMBER",
          "role": {
            "title": "RHEDcloudSecurityIRRole",
            "description": "Incident Response Role for all accounts",
            "stage": "BETA",
            "includedPermissions": [
                "iam.roles.get",
                "iam.roles.list"
            ]
          }
        }
      },
      {
        "name": "custom-maintenance-role",
        "type": "gcp-types/iam-v1:organizations.roles",
        "metadata": {
          "comment": {
            "Metadata": {
                "Tester": "Paul Petersen",
                "Purpose": "The RHEDcloudMaintenanceOperatorRole is assumed by Cloud Administrators who need to maintain accounts.  Example activities include updating CloudFormation templates and adding Service Accounts.",
                "Description": "The RHEDcloudMaintenanceOperatorRole is assumed by Cloud Administrators who need to maintain accounts. The RHEDcloudMaintenanceOperatorRole is configured to be assumed by users who have assumed the RHEDcloudMaintenanceOperatorRole in the master account.",
                "TestStrategy": "The test strategy for the RHEDcloudMaintenanceOperatorRole is to verify that the role exists.",
                "Tests": {
                    "Test": [
                        {
                            "Type": "structural",
                            "Name": "test_maintenance_operator_role.test_exists",
                            "Description": "Verify that the RHEDcloudMaintenanceOperatorRole exists.",
                            "Plan": "Look up the role by name and compare the ARN to the expected result",
                            "ExpectedResult": "Success"
                        },
                        {
                            "Type": "structural",
                            "Name": "test_maintenance_operator_role.test_attached_policies",
                            "Description": "Verify that the x, y and z permissions are associated to the RHEDcloudMaintenanceOperatorRole.",
                            "Plan": "Look up the role by name and inspect the list of associated permissions.",
                            "ExpectedResult": "Success"
                        },
                        {
                            "Type": "structural",
                            "Name": "test_maintenance_operator_role",
                            "Description": "This test performs a series of positive and negative tests to verify the maintenance operator role.",
                            "Plan": "Create a test user, attach the same inline policy to the test user, and perform a series of negative and positive tests to ensure the role is working as expected.",
                            "ExpectedResult": "Success"
                        }
                    ]
                }
              }
            }
        },
        "properties": {
          "parent": "organizations/ORG_ID",
          "roleId": "RHEDcloudMaintenanceOperatorRole_BITBUCKET_BUILD_NUMBER",
          "role": {
            "title": "RHEDcloudMaintenanceOperatorRole",
            "description": "Maintenance Operator Role for all accounts",
            "stage": "BETA",
            "includedPermissions": [
                "iam.roles.get",
                "iam.roles.list"
            ]
          }
        }
      },
      {
        "name": "custom-central-administrator-role",
        "type": "gcp-types/iam-v1:organizations.roles",
        "metadata": {
          "comment": {
            "Metadata": {
                "Tester": "Unassigned",
                "Purpose": "RHEDcloudCentralAdministratorRole will be assumed by Central IT administrators of the AWS Account after they authenticate via SAML with IDP.",
                "Description": "The RHEDcloudCentralAdministratorRole should allow all operations possible to enable central administration of accounts. Presently Information Security has not articulated any specific restrictions that are required for this role, so we may grant access using any appropriate method to achieve necessary access to these accounts to perform administrative functions. Note, however, that this role like all roles in these accounts will be limited by the overarching service control policies implemented at the organization level, which deny access to restricted AWS services and specific AWS service operations.",
                "TestStrategy": "The test strategy for the RHEDcloudCentralAdministratorRole is to verify that the role exists, verify that the expected inline policy is attached to this role, and then implement test cases that assume this role and perform specific allowed operations. No test for prohibited operations are required, because nothing is presently prohibited by the inline policy, although as noted in the description this role is subject to the same service control policy restrictions implemented at the organization level that apply to all principals in the account.",
                "Tests": {
                    "Test": [
                        {
                            "Type": "structural",
                            "Name": "test_central_admin_role_info.test_exists",
                            "Description": "Verify that the RHEDcloudCentralAdministratorRoleExists",
                            "Plan": "Look up the role by name and compare the ARN to the expected result",
                            "ExpectedResult": "Success"
                        },
                        {
                            "Type": "structural",
                            "Name": "test_central_admin_role_info.test_attached_policies",
                            "Description": "Verify that the x, y and z permissions are associated to the RHEDcloudCentralAdministratorRole.",
                            "Plan": "Look up the role by name and inspect the list of associated permissions.",
                            "ExpectedResult": "Success"
                        },
                        {
                            "Type": "functional",
                            "Name": "test_central_admin_role",
                            "Description": "This test performs a series of positive and negative tests to verify the central administrator role.",
                            "Plan": "Create a test user, attach the same inline policy to the test user, and perform a series of negative and positive tests to ensure the role is working as expected.",
                            "ExpectedResult": "Success"
                        }
                    ]
                }
              }
            }
        },
        "properties": {
          "parent": "organizations/ORG_ID",
          "roleId": "RHEDcloudCentralAdministratorRole_BITBUCKET_BUILD_NUMBER",
          "role": {
            "title": "RHEDcloudCentralAdministratorRole",
            "description": "Central Administrator Role for all accounts",
            "stage": "BETA",
            "includedPermissions": [
                "iam.roles.get",
                "iam.roles.list"
            ]
          }
        }
      }
    ]
  }
