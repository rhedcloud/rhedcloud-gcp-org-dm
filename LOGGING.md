# GCP Logging with Deployment Manager

Setting GCP Logging with Deployment Manager.

Covers the following areas:

  * Organization, Folder, and Project Logging Sinks
  * Logging Metrics

Organization Log Sinks require creating a [type provider](https://cloud.google.com/deployment-manager/docs/configuration/type-providers/creating-type-provider).  Type Providers map an API create a custom provider in a GCP project.  Type Providers can be shared across projects.  Type Providers must be provided with an API discovery document.  Here is the document for [logging](https://logging.googleapis.com/$discovery/rest?version=v2).  Here is list of all the [GCP API discovery documents](https://content.googleapis.com/discovery/v1/apis/)

```
gcloud beta deployment-manager type-providers create orgsink --descriptor-url='https://logging.googleapis.com/$discovery/rest?version=v2
   ```
The creation of type provider can be verified with the following command.
```
gcloud beta deployment-manager type-providers list
```
To configure resources with the newly created type provider with deployment manager set the type to match the project name/type provider name:path to API collection.  The attributes to add in are the parent, name, includeChildren, and destination.

Below is an example.
```
      {
            "name": "orgsink",
            "type": "planar-maxim-189715/orgsink2:sinks",
            "properties": {
                "parent": "organizations/482676055061",
                "name": "org-log-sink-all",
                "includeChildren": true,
                "destination": "storage.googleapis.com/org-sink-4587" 
            }
        }
```
