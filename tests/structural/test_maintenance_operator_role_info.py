import pytest

from tests.structural.lib import get_role


@pytest.fixture(scope="module")
def api():
    return "iam"


def test_role_exists(api_client):
    role = get_role(api_client, "RHEDcloudMaintenanceOperatorRole")
    assert role["title"] == "RHEDcloudMaintenanceOperatorRole"
