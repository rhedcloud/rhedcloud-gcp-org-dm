import os


def get_role(api_client, name):
    role_id = "{}_{}".format(name, os.getenv("BITBUCKET_BUILD_NUMBER"))
    role_path = "organizations/{}/roles/{}".format(os.getenv("ORG_ID"), role_id)
    return api_client.organizations().roles().get(
        name=role_path,
    ).execute()
